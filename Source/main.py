import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from database import DataBase
import speech_recognition as sr
from kivy.config import Config
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

Config.set('graphics', 'width', 400)
Config.set('graphics', 'height', 500)

class MainScreen(Screen):
    def btn_clk(self):
        r = sr.Recognizer()
        with sr.Microphone() as source:
            self.lbl.text = 'Say something...'
            audio = r.listen(source)
        try:
            self.lbl.text = 'You said: \n' + r.recognize_google(audio)
        except:
            pass
    def btn_mail(self, mail):
        userMail = 
        passw = 
        recvMail = 
        subject = 
        
        msg = MIMEMultipart()
        msg['From'] = userMail
        msg['To'] = recvMail
        msg['Subject'] = subject

        body = mail
        msg.attach(MIMEText(body,'plain'))
        text = msg.as_string()
        server = smtplib.SMTP('smtp.gmail.com',587)
        server.starttls()
        server.login(userMail,passw)
        server.sendmail(userMail,recvMail,text)
        server.quit()

class LoginScreen(Screen):
    email = ObjectProperty(None)
    password = ObjectProperty(None)

    def loginBtn(self):
        if db.validate(self.email.text, self.password.text):
            MainScreen.current = self.email.text
            self.reset()
            sm.current = "options"
        else:
            invalidLogin()

    def createBtn(self):
        self.reset()
        sm.current = "new"

    def reset(self):
        self.email.text = ""
        self.password.text = "" 

class NewAccountScreen(Screen):
    namee = ObjectProperty(None)
    email = ObjectProperty(None)
    password = ObjectProperty(None)

    def submit(self):
        if self.namee.text != "" and self.email.text != "" and self.email.text.count("@") == 1 and self.email.text.count(".") > 0:
            if self.password != "":
                db.add_user(self.email.text, self.password.text, self.namee.text)

                self.reset()

                sm.current = "login"
            else:
                invalidForm()
        else:
            invalidForm()

    def login(self):
        self.reset()
        sm.current = "login"

    def reset(self):
        self.email.text = ""
        self.password.text = ""
        self.namee.text = ""

class OptionsScreen(Screen):
    pass

class ScreenManagement(ScreenManager):
    pass

def invalidLogin():
    pop = Popup(title='Invalid Login',
                  content=Label(text='Invalid username or password.'),
                  size_hint=(None, None), size=(400, 400))
    pop.open()


def invalidForm():
    pop = Popup(title='Invalid Form',
                  content=Label(text='Please fill in all inputs with valid information.'),
                  size_hint=(None, None), size=(400, 400))

    pop.open()

def existingAccount():
        pop = Popup(title='Existing account',
                      content=Label(text='Account already exists'),
                      size_hint=(None, None), size=(400, 400))
        pop.open()

presentation = Builder.load_file("main.kv")
sm = ScreenManagement()
db = DataBase("users.txt")

screens = [LoginScreen(name="login"), NewAccountScreen(name="new"),OptionsScreen(name="options"),MainScreen(name="main")]
for screen in screens:
    sm.add_widget(screen)

sm.current = "login"
 
class MainApp(App):
    title = "TakeNotes"
    def build(self):
        return sm
    
if __name__ == '__main__':
    MainApp().run()

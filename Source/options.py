import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

from kivy.config import Config
Config.set('graphics', 'resizable', 0)
Config.set('graphics', 'width', 400)
Config.set('graphics', 'height', 500)

class Container(BoxLayout):
    def __init__(self):
        super(Container, self).__init__()

class OptionsApp(App):
    def build(self):
        return Container()

if __name__ == '__main__':
    OptionsApp().run()

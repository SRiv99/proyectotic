# TakeNotes

TakeNotes es una aplicación de reconocimiento de voz que permite a sus usuarios transformar audio en texto.

## Información del Proyecto

Universidad Pontificia Bolivariana

Facultad de Ingeniería

Ingeniería de Sistemas e Informática

Proyecto aplicado en TIC 1

Medellín

2019

## Elaborado Con

* [Python](https://www.python.org/) - Principal lenguaje de programación utilizado en el proyecto.
* [Kivy](https://kivy.org/) - Librería de Pythopn utilizada para el desarrollo de aplicaciones.
* [Buildozer](https://buildozer.readthedocs.io/en/latest/) - herramienta utilizada para empaquetar aplicaciones moviles creadas con python.

## Autores

* **Samuel Rivera** - *Product owner, Scrum master, Equipo Scrum*